#########################################################################
#
# Date: Aug 2003  Authors: Daniel Stoffler, Michel Sanner
#
#       stoffler@scripps.edu
#       sanner@scripps.edu
#
# Copyright: Daniel Stoffler, Michel Sanner, and TSRI
#
#########################################################################

import sys
ed = None
categories = ['Filter', 'Mapper', 'Symmetry', 'Transformation', 'Macro']

def setUp():
    global ed
    from Vision.VPE import VisualProgramingEnvironment
    ed = VisualProgramingEnvironment(name='Vision', withShell=0,)
    ed.root.update_idletasks()
    ed.configure(withThreads=0)


def tearDown():
    ed.exit_cb()
    import gc
    gc.collect()

##########################
## Helper methods
##########################

def pause(sleepTime=0.4):
    from time import sleep
    ed.master.update()
    sleep(sleepTime)
    
def categoryTest(cat):
    from symserv.VisionInterface.SymservNodes import symlib
    ed.addLibraryInstance(symlib, 'ymserv.VisionInterface.SymservNodes', 'symlib')
    ed.root.update_idletasks()
    pause()
    # test the molkit nodes
    lib = 'SymServer'
    libs = ed.libraries
    posx = 150
    posy = 150

    #ed.ModulePages.selectpage(lib)
    ed.root.update_idletasks()
    for node in libs[lib].libraryDescr[cat]['nodes']:
        klass = node.nodeClass
        kw = node.kw
        args = node.args
        netNode = apply( klass, args, kw )
        print 'testing: '+node.name # begin node test
        #add node to canvas
        ed.currentNetwork.addNode(netNode,posx,posy)
        # show widget in node if available:
        widgetsInNode = netNode.getWidgetsForMaster('Node')
        if len( widgetsInNode.items() ):
            if not netNode.isExpanded():
                netNode.toggleNodeExpand_cb()
                ed.root.update_idletasks()
            # and then hide it
            netNode.toggleNodeExpand_cb()
            ed.root.update_idletasks()

        # show widgets in param panel if available:
        widgetsInPanel = netNode.getWidgetsForMaster('ParamPanel')
        if len(widgetsInPanel.items()):
            netNode.paramPanel.show()
            ed.root.update_idletasks()
            #and then hide it
            netNode.paramPanel.hide()
            ed.root.update_idletasks()

        # and now delete the node
        ed.currentNetwork.deleteNodes([netNode])
        ed.root.update_idletasks()
        print 'passed: '+node.name # end node test

##########################
## Tests
##########################

def test_01_loadSymLib():
    from symserv.VisionInterface.SymservNodes import symlib
    ed.addLibraryInstance(symlib, 'ymserv.VisionInterface.SymservNodes', 'symlib')
    ed.root.update_idletasks()
    pause()


# Split testing of the nodes in different tests (by category). 

def test_02_FilterCategory():
    categoryTest("Filter")


def test_03_MapperCategory():
    categoryTest("Mapper")

def test_04_SymmetriesCategory():
    categoryTest("Symmetry")

def test_05_TransformationsCategory():
    categoryTest("Transformation")

def test_06_MacrosCategory():
    categoryTest("Macro")
        

def old_test_02_allSymlibNodes():
    from symserv.VisionInterface.SymservNodes import symlib
    ed.addLibraryInstance(symlib, 'ymserv.VisionInterface.SymservNodes', 'symlib')
    ed.root.update_idletasks()
    pause()
    # test the molkit nodes
    lib = 'SymServer'
    libs = ed.libraries
    posx = 150
    posy = 150

    #ed.ModulePages.selectpage(lib)
    ed.root.update_idletasks()
    for cat in libs[lib].libraryDescr.keys():
        print "category :", cat
        for node in libs[lib].libraryDescr[cat]['nodes']:
            klass = node.nodeClass
            kw = node.kw
            args = node.args
            netNode = apply( klass, args, kw )
            print 'testing: '+node.name # begin node test
            #add node to canvas
            ed.currentNetwork.addNode(netNode,posx,posy)
            # show widget in node if available:
            widgetsInNode = netNode.getWidgetsForMaster('Node')
            if len( widgetsInNode.items() ):
                if not netNode.isExpanded():
                    netNode.toggleNodeExpand_cb()
                    ed.root.update_idletasks()
                # and then hide it
                netNode.toggleNodeExpand_cb()
                ed.root.update_idletasks()

            # show widgets in param panel if available:
            widgetsInPanel = netNode.getWidgetsForMaster('ParamPanel')
            if len(widgetsInPanel.items()):
                netNode.paramPanel.show()
                ed.root.update_idletasks()
                #and then hide it
                netNode.paramPanel.hide()
                ed.root.update_idletasks()

            # and now delete the node
            ed.currentNetwork.deleteNodes([netNode])
            ed.root.update_idletasks()
            print 'passed: '+node.name # end node test


