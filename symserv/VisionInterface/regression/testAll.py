import sys
from mglutil.regression import testplus

import test_symnodes

harness = testplus.TestHarness( "testAll_symserv_ViPEr",
                                funs = [],
                                dependents = [test_symnodes.harness,
                                              ],
                                )

if __name__ == '__main__':
    testplus.chdir()
    print harness
    sys.exit( len( harness))
