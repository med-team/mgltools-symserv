#########################################################################
#
# Date: Jun 2002 Authors: Daniel Stoffler
#
#    stoffler@scripps.edu
#    sanner@scripps.edu
#
# Copyright:  Daniel Stoffler, and TSRI
#
#########################################################################

import sys, string
from mglutil.regression import testplus
from Vision.VPE import VisualProgramingEnvironment

from NetworkEditor.simpleNE import NetworkNode
from time import sleep
from symserv.VisionInterface.SymservNodes import SymTransNE, SymRotNE, SymMergeNE

ed = None
withThreads = 1 # default is: multi-threading on

# allow for additional user input
if len(sys.argv):
    for myArg in sys.argv[1:]:
        if myArg[:11] == 'withThreads':
            withThreads = int(string.strip(myArg)[-1])
           

def pause(sleepTime=0.2):
    ed.master.update()
    sleep(sleepTime)


def startEditor():
    global ed
    ed = VisualProgramingEnvironment(name='Vision')
    ed.root.update_idletasks()
    ed.configure(withThreads=withThreads)


def test_loadSymlib():
    from symserv.VisionInterface.SymservNodes import symlib
    ed.addLibrary(symlib)
    

def quitEditor():
    ed.master.after(1000, ed.exit_cb )


def test_allSymNodes():
    # test the symserv nodes
    libs = ed.libraries
    posx = 150
    posy = 150
    for lib in libs.keys():
        ed.ModulePages.selectpage(lib)
        ed.root.update_idletasks()
        for cat in libs[lib].libraryDescr.keys():
            for node in libs[lib].libraryDescr[cat]['nodes']:
                klass = node.nodeClass
                kw = node.kw
                args = node.args
                netNode = apply( klass, args, kw )
                print 'testing: '+node.name # begin node test
                #add node to canvas
                ed.currentNetwork.addNode(netNode,posx,posy)
                # show widget in node if available:
                widgetsInNode = netNode.getWidgetsForMaster('Node')
                if len(widgetsInNode.items()) is not None:
                    for port,widget in widgetsInNode.items():
                        netNode.createOneWidget(port)
                        ed.root.update_idletasks()
                    # and then hide it
                    for port,widget in widgetsInNode.items():
                        netNode.hideInNodeWidget(port.widget)
                        ed.root.update_idletasks()

                # show widgets in param panel if available:
                widgetsInNode = netNode.getWidgetsForMaster('ParamPanel')
                if len(widgetsInPanel.items()):
                    netNode.paramPanel.show()
                    ed.root.update_idletasks()

                    #and then hide it
                    netNode.paramPanel.hide()
                    ed.root.update_idletasks()
                        
                # and now delete the node
                ed.currentNetwork.deleteNodes([netNode])
                ed.root.update_idletasks()
                
                print 'passed: '+node.name # end node test


def test_SymTrans():
    global ed
    nodeT = SymTransNE()
    ed.currentNetwork.addNode(nodeT,100,100)
    pause()
    ed.currentNetwork.deleteNodes([nodeT])
    pause()


def test_SymRot():
    global ed
    nodeR = SymRotNE()
    ed.currentNetwork.addNode(nodeR,100,100)
    pause()
    ed.currentNetwork.deleteNodes([nodeR])
    pause()


def test_SymMerge():
    global ed
    nodeM = SymMergeNE()
    ed.currentNetwork.addNode(nodeM,100,100)
    pause()
    ed.currentNetwork.deleteNodes([nodeM])
    pause()

   

    
harness = testplus.TestHarness( __name__,
                                connect = startEditor,
                                funs = testplus.testcollect( globals()),
                                disconnect = quitEditor
                                )

if __name__ == '__main__':
    print harness
    sys.exit( len( harness))
